package com.address.http;

import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.HashMap;
import java.util.Map;

public class getTest {
    public static void main(String[] args) throws IOException {
        Map<String,String> mp = new HashMap<>();
        mp.put("address","江苏省南京市江宁区秣陵街道胜太社区双龙大道1539号21世纪国际公寓西区66栋1单元201");
        String res = HttpClientUtil.doGet("http://localhost:8000/standard",mp);
//        String res = HttpClientUtil.doGet("http://localhost:8090/test2",mp);
        System.out.println(res);

        mp.put("id","123");
        mp.put("name","张三");

        SimpleDateFormat formatter= new SimpleDateFormat("yyyy-MM-dd 'at' HH:mm:ss z");
        Date date = new Date(System.currentTimeMillis());
        System.out.println("开始时间" + formatter.format(date));
        for(int i=0;i<5000;i++){
            res = HttpClientUtil.doGet("http://localhost:8000/standard",mp);
        }
        date = new Date(System.currentTimeMillis());
        System.out.println("结束时间" + formatter.format(date));
    }
}
