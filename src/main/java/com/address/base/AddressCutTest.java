package com.address.base;

import org.address.AddressCut;
import org.address.entity.Word;


import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;

public class AddressCutTest {
    public static void main(String[] args) {
        AddressCut ss = new AddressCut();
        ss.setUserDefineDic( "新平村","community",100);
        ss.setUserDefineDic( "委会","n",1);
        List<Word> words;
        //正常地址
        words = ss.cutAddress("四川省自贡市贡井区成佳镇新平村委会4组-1-4");
        System.out.println(words);
        //城市名修正
        words = ss.cutAddress("湖北省武汉汉阳区汉阳大道10号花果山5栋1单元101户");
        System.out.println(words);
        // 省份修正 城市修正
        words = ss.cutAddress("湖北武汉汉阳区汉阳大道10号花果山5号楼1单元101室");
        System.out.println(words);
        // 城市修正
        words = ss.cutAddress("江苏省镇江市学府路花果小区");
        System.out.println(words);
        // 省份补全 城市补全
        words = ss.complete(ss.cutAddress("佛祖岭社区汉阳大道10号花果山5号楼1单元101室"));
        System.out.println(words);
        System.out.println(ss.complete(ss.cutAddress("湖北省\t武汉市\t汉阳\t汉阳-七里庙-永丰路\t永丰乡政府小区\n")));
        System.out.println(ss.complete(ss.cutAddress("湖北省 武汉市 汉阳永丰路 永丰乡政府小区")));
        System.out.println(ss.complete(ss.cutAddress("江苏省南京市江宁区上善路6号 汤山颐和府3栋一单元802室")));

        SimpleDateFormat formatter= new SimpleDateFormat("yyyy-MM-dd 'at' HH:mm:ss z");
        Date date = new Date(System.currentTimeMillis());
        System.out.println("开始时间 : " + formatter.format(date));
        int cnt=0;
        for(int i=0;i<500;i++){
            ss.complete(ss.cutAddress("湖北省 武汉市 汉阳永丰路 永丰乡政府小区"));
        }
        System.out.println("解析地址条数 : " + cnt + "条");
        date = new Date(System.currentTimeMillis());
        System.out.println("结束时间 : " + formatter.format(date));
    }
}
