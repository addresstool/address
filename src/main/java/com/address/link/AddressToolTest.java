package com.address.link;


import org.address.AddressTool;
import org.address.DataTable;


import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.HashMap;

public class AddressToolTest {
    public static void main(String[] args) throws Exception {

        AddressTool ss = new AddressTool();
        DataTable data = new DataTable();
        // 汉东省京州市天山区华山街道孽海社区卧龙大道元气森林香溪谷
        HashMap<String,String> mp1 = new HashMap<>();
        mp1.put("id","11111");
        mp1.put("province","汉东省");
        mp1.put("city","京州市");
        mp1.put("county","天山区");
        mp1.put("town","华山街道");
        mp1.put("community","孽海社区");
        mp1.put("road","卧龙大道");
        mp1.put("near_roads","天地大道中路");  // 道路别名
        mp1.put("road_no","1");
        mp1.put("aoi","元气森林香溪谷");
        mp1.put("other_name","香溪谷#溪谷小区");
        data.addAddressDic(mp1);
        HashMap<String,String> mp2 = new HashMap<>();
        mp2.put("id","22222");
        mp2.put("province","汉东省");
        mp2.put("city","京州市");
        mp2.put("county","天山区");
        mp2.put("town","华山街道");
        mp2.put("community","孽海社区");
        mp2.put("road","卧龙大道");
        mp2.put("road_no","1");
        mp2.put("aoi","元气森林卧龙居");
        mp1.put("other_name","卧龙居#龙居小区");
        data.addAddressDic(mp2);


        System.out.println("用户地址 读取完毕！！！ ");
        // 补全行3级政区 对于行政完整的地址无需此操作
//        data.completion();
//        // 标准地址库修复，比如用户只有户室级地址，此方法为用户补充楼栋级和aoi级地址，当然，如果用户有自己已经标准化好的地址库，可以省略此方法
//        data.addressFix();
        data.initData(ss);

//        as.close();
        SimpleDateFormat formatter= new SimpleDateFormat("yyyy-MM-dd 'at' HH:mm:ss z");
        Date date1 = new Date(System.currentTimeMillis());
        System.out.print(formatter.format(date1));
        System.out.print("   ");
        System.out.println("用户地址 初始化完毕！！！ ");

        System.out.println(ss.getStdAddress("汉东省京州市天山区华山街道孽海社区卧龙大道元气森林香溪谷"));
        System.out.println(ss.getStdAddress("卧龙大道元气森林香溪谷"));
        System.out.println(ss.getStdAddress("元气森林香溪谷"));
        System.out.println(ss.getStdAddress("汉东省京州市天山区华山街道孽海社区卧龙大道1号"));
        System.out.println(ss.getStdAddress("香溪谷"));
        System.out.println(ss.getStdAddress("卧龙居小区"));
        System.out.println(ss.getStdAddress("汉东省京州市天山区华山街道孽海社区燊海香溪谷"));


        Date date = new Date(System.currentTimeMillis());
        System.out.println(formatter.format(date));
        for(int i=0;i<500;i++){
            ss.getStdAddress("汉东省京州市天山区华山街道孽海社区卧龙大道元气森林香溪谷");
        }
        date = new Date(System.currentTimeMillis());
        System.out.println(formatter.format(date));
//        as.close();
    }


}
