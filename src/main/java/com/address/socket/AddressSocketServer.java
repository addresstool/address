package com.address.socket;

import org.address.AddressTool;
import org.address.entity.StandardAddress;


import java.io.IOException;
import java.io.InputStream;
import java.net.ServerSocket;
import java.net.Socket;
import java.nio.charset.Charset;
import java.nio.charset.StandardCharsets;

public class AddressSocketServer {
    AddressTool ss = new AddressTool();

    public AddressSocketServer() throws Exception {
        ss.loadFromJson("D:\\","address4.json");
    }
    /**
     * 服务端程序
     */
    public void server() throws IOException {
        // 为了简单起见，所有的异常信息都往外抛
        int port = 9966;
        // 定义一个ServiceSocket监听在端口9966上
        ServerSocket server = new ServerSocket(port);
        System.out.println("等待与客户端建立连接...");
        while (true) {
            // server尝试接收其他Socket的连接请求，server的accept方法是阻塞式的
            Socket socket = server.accept();
            /**
             * 我们的服务端处理客户端的连接请求是同步进行的， 每次接收到来自客户端的连接请求后，
             * 都要先跟当前的客户端通信完之后才能再处理下一个连接请求。 这在并发比较多的情况下会严重影响程序的性能，
             * 为此，我们可以把它改为如下这种异步处理与客户端通信的方式
             */
            // 每接收到一个Socket就建立一个新的线程来处理它
            new Thread(new Task(socket)).start();
        }
        // server.close();

    }


    class Task implements Runnable {

        private Socket socket;
        /**
         * 构造函数
         */
        public Task(Socket socket) {
            this.socket = socket;
        }

        @Override
        public void run() {
            try {
                handlerSocket(socket);
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
    }

    private void handlerSocket(Socket socket) throws IOException {


        System.out.println("连接成功！");
        while (true) {
            try {
                // 获取客户端输入流
                InputStream inputStream = socket.getInputStream();
                byte[] bytes = new byte[1024];
                int read = inputStream.read(bytes);

                String addr = new String(bytes, 0, read, Charset.defaultCharset());
                // 客户端发来的消息
//                System.out.println("客户端：" + addr);
                StandardAddress address = ss.getStdAddress(addr);
                // 给客户端发端东西
                socket.getOutputStream().write(address.toString().getBytes(StandardCharsets.UTF_8));
            } catch (Exception e) {
//                System.out.println("无客户端链接！！！");
                socket.close();
                break;
            }
        }
    }
    public static void main(String[] args) throws Exception {
        AddressSocketServer tcpSocketServer = new AddressSocketServer();
        tcpSocketServer.server();;
    }
}
