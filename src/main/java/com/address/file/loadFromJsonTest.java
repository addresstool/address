package com.address.file;

import org.address.AddressTool;
import org.address.DataTable;
import org.address.entity.StandardAddress;

import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.HashMap;
import java.util.Map;

public class loadFromJsonTest {
    public static void main(String[] args) throws Exception {
        AddressTool ss = new AddressTool();
        DataTable as = new DataTable();
        as.loadFromJson("D:\\ideacode\\address\\src\\main\\resources\\","wuhan.json");
        System.out.println("用户地址 读取完毕！！！ ");
        as.initData(ss);
        SimpleDateFormat formatter= new SimpleDateFormat("yyyy-MM-dd 'at' HH:mm:ss z");
        Date date1 = new Date(System.currentTimeMillis());
        System.out.print(formatter.format(date1));
        System.out.print("   ");
        System.out.println("用户地址 初始化完毕！！！ ");

        Map<String,String> mp = new HashMap<>();
        // 忽略地理要素，被忽略的地理要素不会作为关键词进行关联匹配
//        mp.put("ignore","town,community");
        System.out.println(ss.getStdAddress("武汉市锦绣龙城"));
        System.out.println(ss.getStdAddress("湖北省武汉市江夏高新四路1号万科魅力之城",mp));
        System.out.println(ss.getStdAddress("湖北省武汉市江夏高新四路万科魅力之城"));
        System.out.println(ss.getStdAddress("武汉市魅力之城"));
        System.out.println(ss.getStdAddress("武汉市万科魅力"));



        Date date = new Date(System.currentTimeMillis());
        System.out.println(formatter.format(date));
        for(int i=0;i<50000;i++){
            ss.getStdAddress("湖北省武汉市江夏高新四路万科魅力之城");
        }
        date = new Date(System.currentTimeMillis());
        System.out.println(formatter.format(date));
        as.close();
    }
}
