package com.address.poisearch;

import org.address.PoiSearch;

import java.text.SimpleDateFormat;
import java.util.Date;

public class PoiSearchTest {
    public static void main(String[] args) {
        PoiSearch ps = new PoiSearch();
        ps.addPoi("汉东省京州市蔡店乡木兰清凉寨内简餐冷饮");
        ps.addPoi("汉东省京州市蔡店街82号陈记炸酱面美食城(蔡店街店)");
        ps.addPoi("汉东省京州市蔡店南街与蔡店西街交叉口北50米华美酒楼(蔡店南街)");
        ps.addPoi("汉东省京州市蔡店街88号华龙酒店");
        ps.addPoi("汉东省京州市新车站蔡店街67号天怀餐馆");
        ps.addPoi("汉东省京州市138乡道附近小红早点");
        ps.addPoi("汉东省京州市蔡店乡道士冲村锦里沟内山村作坊");
        ps.addPoi("汉东省京州市武汉锦里沟景区入口处锦里沟土家风味馆");
        ps.addPoi("汉东省京州市蔡店镇锦里沟风景区内刘三姐农家乐");
        ps.addPoi("汉东省京州市田家湾土家风情园");
        ps.addPoi("汉东省京州市双河村刘家湾双河村村委会旁(罗家祠堂北)双河渔庄");
        ps.addPoi("汉东省京州市姚集大道223号派乐汉堡");
        ps.addPoi("汉东省京州市双龙路与百业街交叉口东150米金福缘餐厅");
        ps.addPoi("汉东省京州市双龙路与百业街交叉口东200米实惠餐厅");
        ps.addPoi("汉东省京州市姚家集街八角门路1号铁脑壳农家乐");
        ps.addPoi("汉东省京州市012县道与013乡道交叉口北50米向家嘴土菜馆");
        ps.addPoi("汉东省京州市长轩岭街朱家山湾17号兵兵农家乐饭庄");
        ps.addPoi("汉东省京州市长轩岭镇石门山木兰天池度假酒店木兰天池将军府");
        ps.addPoi("汉东省京州市长轩岭镇石门山木兰天池度假酒店木兰天池将军府","花木兰故居","武汉花木兰故居");

//        ps.search("阿达西烧烤店");
        System.out.println(ps.search("汉东省京州市蔡甸区天怀餐馆"));
        System.out.println(ps.search("汉东省京州市三姐农家乐"));
        System.out.println(ps.search("汉东省京州市度假酒店木兰天池"));
        System.out.println(ps.search("汉东省京州市花木兰故居"));

        SimpleDateFormat formatter= new SimpleDateFormat("yyyy-MM-dd 'at' HH:mm:ss z");
        Date date = new Date(System.currentTimeMillis());
        System.out.println("开始时间" + formatter.format(date));
        for(int i=0;i<50000;i++){
            ps.search("风景区三姐农家乐");
        }
        date = new Date(System.currentTimeMillis());
        System.out.println("结束时间" + formatter.format(date));
    }
}
